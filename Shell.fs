namespace Playsole

module Shell =
    open Terminal.Gui
    open Terminal.Gui.Elmish
    open Player

    open Types
    open Fs

    type State =
        { Page: Page
          OverviewState: Overview.State
          PlaylistState: Playlist.State
          MusicLibraryState: MusicLibrary.State }


    type Msg =
        | Quit
        | NavigateTo of Page
        | OverviewMsg of Overview.Msg
        | PlaylistMsg of Playlist.Msg
        | MusicLibraryMsg of MusicLibrary.Msg

        | AddToPlaylist of AppFile
        | AddToPlaylistSuccess of unit

        | PlaySong of AppFile

    let init () =
        let (playlist, playlistCmd) = Playlist.init ()
        let (musiclibrary, musicLibraryCmd) = MusicLibrary.init ()
        { Page = Overview
          OverviewState = Overview.init ()
          PlaylistState = playlist
          MusicLibraryState = musiclibrary },
        Cmd.batch
            [ Cmd.map PlaylistMsg playlistCmd
              Cmd.map MusicLibraryMsg musicLibraryCmd ]

    let handleMusicLibraryExternal (msg: Option<MusicLibrary.ExternalMsg>) =
        match msg with
        | None -> Cmd.none
        | Some msg ->
            match msg with
            | MusicLibrary.ExternalMsg.AddToPlaylist file -> Cmd.ofMsg (AddToPlaylist file)

    let handlePlaylistExternal (msg: Option<Playlist.ExternalMsg>) =
        match msg with
        | None -> Cmd.none
        | Some msg ->
            match msg with
            | Playlist.ExternalMsg.PlaySong file -> Cmd.ofMsg (PlaySong file)

    let update (msg: Msg) (state: State) (player: IPlayer) =
        match msg with
        | Quit ->
            Program.quit ()
            state, Cmd.none
        | NavigateTo page ->
            let cmd =
                match page with
                | MusicLibrary -> Cmd.map MusicLibraryMsg (Cmd.ofMsg MusicLibrary.Msg.LoadMusicLibrary)
                | _ -> Cmd.none

            { state with Page = page }, cmd
        | OverviewMsg overviewmsg ->
            let overviewstate, cmd =
                Overview.update overviewmsg state.OverviewState

            { state with
                  OverviewState = overviewstate },
            Cmd.map OverviewMsg cmd
        | PlaylistMsg playlistmsg ->
            let playliststate, cmd, externalmsg =
                Playlist.update playlistmsg state.PlaylistState

            let mapped = handlePlaylistExternal externalmsg
            { state with
                  PlaylistState = playliststate },
            Cmd.batch [ Cmd.map PlaylistMsg cmd; mapped ]
        | PlaySong file ->
            match player.Playlist
                  |> List.tryFindIndex (fun f -> f.Path = file.Path) with
            | Some index -> player.Play index
            | None -> ()
            state, Cmd.none

        | MusicLibraryMsg musiclibrarymsg ->
            let musiclibrarystate, cmd, externalMsg =
                MusicLibrary.update musiclibrarymsg state.MusicLibraryState

            let mapped = handleMusicLibraryExternal externalMsg
            { state with
                  MusicLibraryState = musiclibrarystate },
            Cmd.batch [ Cmd.map MusicLibraryMsg cmd; mapped ]
        | AddToPlaylist file ->
            let cmd =
                Cmd.OfTask.perform player.AddToPlaylistAsync file AddToPlaylistSuccess

            state, cmd
        | AddToPlaylistSuccess ->
            let playlist = player.Playlist

            let plState =
                { state.PlaylistState with
                      Playlist = playlist }

            { state with PlaylistState = plState }, Cmd.none

    let appMenu (state: State) (dispatch: Msg -> unit) =
        menuBar [ menuBarItem "Menu" [ menuItem "Quit" "- quits the application" (fun _ -> dispatch Quit) ] ]

    let windowNav (state: State) (dispatch: Msg -> unit) =
        let styles =
            Styles
                [ Pos(AbsPos 0, AbsPos 1)
                  Dim(Fill, PercentDim 16.) ]

        let btnStyles order =
            Styles
                [ Pos(PercentPos(order * 12.), AbsPos 0)
                  Dim(Fill, Fill) ]

        window [ styles; Title "Nav" ]
            [ button
                [ btnStyles 0.
                  Text "Overview"
                  OnClicked(fun _ -> dispatch (NavigateTo Overview)) ]
              button
                  [ btnStyles 1.
                    Text "Playlist"
                    OnClicked(fun _ -> dispatch (NavigateTo Playlist)) ]
              button
                  [ btnStyles 2.
                    Text "Music Library"
                    OnClicked(fun _ -> dispatch (NavigateTo MusicLibrary)) ] ]

    let appWindow (state: State) (dispatch: Msg -> unit) =
        let title =
            match state.Page with
            | Overview -> "Overview"
            | Playlist -> "Playlist"
            | MusicLibrary -> "Music Library"

        let content =
            match state.Page with
            | Overview -> Overview.view state.OverviewState (OverviewMsg >> dispatch)
            | Playlist -> Playlist.view state.PlaylistState (PlaylistMsg >> dispatch)
            | MusicLibrary -> MusicLibrary.view state.MusicLibraryState (MusicLibraryMsg >> dispatch)

        let styles =
            Styles
                [ Pos(AbsPos 0, PercentPos 18.)
                  Dim(Fill, FillMargin 5) ]

        window [ styles; Title title ] content

    let mediaPlayer (state: State) (dispatch: Msg -> unit) =
        let styles =
            Styles
                [ Pos(AbsPos 0, PercentPos 80.)
                  Dim(Fill, Fill) ]

        let content: list<View> = []

        window [ styles; Title "Player" ] content

    let view (state: State) (dispatch: Msg -> unit) =
        page
            [ appMenu state dispatch
              windowNav state dispatch
              appWindow state dispatch
              mediaPlayer state dispatch ]

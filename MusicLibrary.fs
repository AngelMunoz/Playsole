namespace Playsole

module MusicLibrary =
    open Terminal.Gui
    open Terminal.Gui.Elmish
    open Fs
    open Fs.FileSystem

    type State = { Library: Option<AppFolder> }


    type ExternalMsg = AddToPlaylist of AppFile

    type Msg =
        | LoadMusicLibrary
        | LoadMusicLibrarySuccess of AppFolder
        | LoadMusicLibraryErr of exn
        | AddToPlaylist of AppFile


    let init () =
        { Library = None }, Cmd.ofMsg LoadMusicLibrary

    let update (msg: Msg) (state: State) =
        match msg with
        | LoadMusicLibrary ->
            state, Cmd.OfTask.either getMusicLibraryAsync () LoadMusicLibrarySuccess LoadMusicLibraryErr, None
        | LoadMusicLibrarySuccess appfolder -> { state with Library = Some appfolder }, Cmd.none, None
        | LoadMusicLibraryErr err ->
            eprintfn "%O" err
            state, Cmd.none, None
        /// External Messages
        | AddToPlaylist file -> state, Cmd.none, Some(ExternalMsg.AddToPlaylist file)

    let private childcontent (files: Option<seq<AppFile>>) (dispatch: Msg -> unit) =
        let content =
            match files with
            | Some files ->
                let files =
                    files
                    |> Seq.map (fun file -> (file, file.FileName))
                    |> List.ofSeq

                [ Items files
                  OnChanged(AddToPlaylist >> dispatch) ]
            | None -> []

        listView content


    let view (state: State) (dispatch: Msg -> unit): list<View> =
        let title =
            match state.Library with
            | Some lib -> sprintf "%s - %s" lib.Name lib.Path
            | None -> ""

        let appfile =
            match state.Library with
            | Some lib -> lib.Children
            | None -> None

        [ frameView [ Text title ] [ childcontent appfile dispatch ] ]

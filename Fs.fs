namespace Playsole

/// <summary>
/// This module is an abstraction of the underlying OS File Access.
/// In Windows we use the WinRT API while on the other platforms we use the dotnet API
/// </summary>
module Fs =
    open System
    open System.Threading.Tasks

    open FSharp.Control.Tasks.Builders

    open Types
    open Helpers

    type MusicProperties =
        { Title: Option<string>
          AlbumArtist: Option<string>
          AlbumTitle: Option<string>
          Artist: Option<string>
          TrackNumber: Option<uint32> }

    type AppFile =
        { Path: string
          FileName: string
          Title: Option<string>
          AlbumArtist: Option<string>
          AlbumTitle: Option<string>
          Artist: Option<string>
          TrackNumber: Option<uint32> }

    type AppFolder =
        { Name: string
          Path: string
          Children: Option<seq<AppFile>> }

    /// <summary>
    /// Common interface to abstract the file access to the underlying OS.
    ///
    /// If either of the WinRT API implemenation or the dotnet implemenation is Async
    ///
    /// (returns a <see cref="System.Threading.Tasks.Task"/> or <see cref="Windows.Foundation.IAsyncAction"/> or <see cref="Windows.Foundation.IAsyncOperation"/>)
    ///
    /// the method sould be marked as Async and return a Task (always prefer tasks for this interface)
    /// </summary>
    type IFileSystem =
        /// Finds the Music Library as defined in
        /// <see cref="System.Environment.SpecialFolder">SpecialFolder</see>(dotnet) or
        /// <see cref="Windows.Storage.KnownFolders">KnownFolders</see>(WinRT)
        /// Implementations should Provide an existing directory where music is usually stored
        abstract GetMusicLibraryAsync: unit -> Task<AppFolder>
        abstract GetFileAsync: path:string -> Task<AppFile>
        abstract GetFolderAsync: path:string -> Task<AppFolder>
        abstract GetMusicPropertiesAsync: AppFile -> Task<MusicProperties>

    /// Provides cross-platform implementations for the IFileSystem interface and any helper that implementations should require.
    ///
    /// Every Type/Function should be compatible with Linux/MacOS
    module private CrossPlat =
        open System
        open System.IO

        /// Implements <see cref="Playsole.Fs.IFileSystem"/>
        type CrossPlatFileSystem() =

            static member private GetAppFolder(dir: DirectoryInfo) =
                let files =
                    dir.EnumerateFileSystemInfos()
                    |> Seq.filter (fun file -> file.Extension = ".mp3")
                    |> Seq.map (fun file ->
                        { FileName = file.Name
                          Path = file.FullName
                          Title = None
                          AlbumArtist = None
                          AlbumTitle = None
                          Artist = None
                          TrackNumber = None })

                { Name = "Music Library"
                  Path = dir.FullName
                  Children = Some files }

            interface IFileSystem with
                member __.GetMusicLibraryAsync(): Task<AppFolder> =
                    let path =
                        Environment.GetFolderPath(Environment.SpecialFolder.MyMusic)

                    let dir = DirectoryInfo(path)
                    Task.FromResult(CrossPlatFileSystem.GetAppFolder(dir))

                member __.GetFileAsync(path: string): Task<AppFile> =
                    task {
                        let file = FileInfo(path)
                        return { FileName = file.Name
                                 Path = file.FullName
                                 Title = None
                                 AlbumArtist = None
                                 AlbumTitle = None
                                 Artist = None
                                 TrackNumber = None }
                    }

                member __.GetFolderAsync(path: string): Task<AppFolder> =
                    let dir = DirectoryInfo(path)
                    Task.FromResult(CrossPlatFileSystem.GetAppFolder(dir))

                member __.GetMusicPropertiesAsync(appfile: AppFile): Task<MusicProperties> =
                    Console.ForegroundColor <- ConsoleColor.Yellow
                    Console.WriteLine("Warning: MusicProperties Are Not Supported in Linux Yet")
                    Console.ResetColor()

                    let props =
                        { Title = None
                          AlbumArtist = None
                          AlbumTitle = None
                          Artist = None
                          TrackNumber = None }

                    Task.FromResult(props)

    /// <summary>
    /// Provides cross-platform implementations for the IFileSystem interface and any helper that implementations should require.
    ///
    /// Every Type/Function should be compatible with Windows 10
    /// </summary>
    /// <remarks>If a WinRT API offers the functionality of a dotnet API for this module the WinRT API should be preferred</remarks>
    module private Win10 =
        open Windows.Storage
        open Windows.Storage.Search

        type WindowsFileSystem() =

            static member private GetAppFolderAsync(dir: string) =
                task {
                    let! dir = StorageFolder.GetFolderFromPathAsync(dir).AsTask()

                    let opts =
                        QueryOptions(CommonFileQuery.OrderByMusicProperties, seq { ".mp3" })

                    let query = dir.CreateFileQueryWithOptions(opts)
                    let! files = query.GetFilesAsync().AsTask()

                    let tasks =
                        files
                        |> Seq.toArray
                        |> Array.Parallel.map (fun file ->
                            task {
                                let! props = file.Properties.GetMusicPropertiesAsync().AsTask()
                                return { Path = file.Path
                                         FileName = file.DisplayName
                                         Title = Some(props.Title)
                                         AlbumArtist = Some(props.AlbumArtist)
                                         AlbumTitle = Some(props.Album)
                                         Artist = Some(props.Artist)
                                         TrackNumber = Some(props.TrackNumber) }
                            })

                    let! result = Task.WhenAll(tasks)
                    return { Name = "Music Library"
                             Path = dir.Path
                             Children = Some(result |> seq) }
                }

            interface IFileSystem with
                member __.GetMusicLibraryAsync(): Task<AppFolder> =
                    WindowsFileSystem.GetAppFolderAsync(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic))

                member __.GetFileAsync(path: string): Task<AppFile> =
                    task {
                        let! file = StorageFile.GetFileFromPathAsync(path).AsTask()
                        let! props = file.Properties.GetMusicPropertiesAsync().AsTask()
                        return { Path = file.Path
                                 FileName = file.DisplayName
                                 Title = Some(props.Title)
                                 AlbumArtist = Some(props.AlbumArtist)
                                 AlbumTitle = Some(props.Album)
                                 Artist = Some(props.Artist)
                                 TrackNumber = Some(props.TrackNumber) }
                    }

                member __.GetFolderAsync(path: string): Task<AppFolder> = WindowsFileSystem.GetAppFolderAsync(path)

                member __.GetMusicPropertiesAsync(appfile: AppFile): Task<MusicProperties> =
                    task {
                        let! fs = StorageFile.GetFileFromPathAsync(appfile.Path).AsTask()
                        let! props = fs.Properties.GetMusicPropertiesAsync().AsTask()
                        return { Title = Option.ofObj (props.Title)
                                 AlbumArtist = Option.ofObj (props.AlbumArtist)
                                 AlbumTitle = Option.ofObj (props.Album)
                                 Artist = Option.ofObj (props.Artist)
                                 TrackNumber = Some props.TrackNumber }
                    }

    /// <summary>
    /// Public module that encapsulates the IFileSystem functionality in callable functions to make it feel more
    /// F#'ish, since the IFileSystem implementations are not stateful
    /// </summary>
    module FileSystem =
        let private getPlatformFileSystem (platform: AppPlatform): IFileSystem =
            match platform with
            | Linux
            | MacOS -> CrossPlat.CrossPlatFileSystem() :> IFileSystem
            | Windows -> Win10.WindowsFileSystem() :> IFileSystem

        let private getFileSystem: Lazy<IFileSystem> =
            let platform = getPlatform ()
            lazy (getPlatformFileSystem platform)

        let getMusicLibraryAsync () =
            let fs = getFileSystem
            fs.Value.GetMusicLibraryAsync()

        let getFileAsync (path: string): Task<AppFile> =
            let fs = getFileSystem
            fs.Value.GetFileAsync(path)

        let getFolderAsync (path: string): Task<AppFolder> =
            let fs = getFileSystem
            fs.Value.GetFolderAsync(path)

        let getMusicPropertiesAsync (appfile: AppFile): Task<MusicProperties> =
            let fs = getFileSystem
            fs.Value.GetMusicPropertiesAsync(appfile)

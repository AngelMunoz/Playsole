namespace Playsole

open Terminal.Gui.Elmish
open Terminal.Gui

module Main =

    [<EntryPoint>]
    let main argv =
        Application.Init()
        let mutable ids: list<NStack.ustring> = list.Empty
        /// hacky way to ensure that the color scheme is applied
        /// since the library does not support applying colors to every control/element/component
        let ensureColorscheme _ =
            match Option.ofObj (Application.Current) with
            | Some top ->
                match ids |> List.tryFind (fun id -> top.Id = id) with
                | None ->
                    ids <- top.Id :: ids
                    top.ColorScheme.Normal <- Attribute.Make(Color.Brown, Color.Black)
                | Some _ -> ()
            | None -> ()

        ensureColorscheme ()

        let updateWithPlayer (msg: Shell.Msg) (state: Shell.State) =
            Shell.update msg state Player.getPlayer.Value

        Application.Iteration.Subscribe(ensureColorscheme)
        |> ignore
        Program.mkProgram Shell.init updateWithPlayer Shell.view
        |> Program.run
        0 // return an integer exit code

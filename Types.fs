namespace Playsole

module Types =
    type Page =
        | Overview
        | Playlist
        | MusicLibrary

    type AppPlatform =
        | Windows
        | Linux
        | MacOS

namespace Playsole

/// Handles everything related to the music player either LibVLCSharp or the WinRT API
module Player =

    open System
    open System.Threading.Tasks

    open FSharp.Control.Tasks.Builders

    open Types
    open Helpers
    open Fs

    type IPlayer =
        inherit IDisposable
        abstract Play: unit -> unit
        abstract Play: int -> unit
        abstract Pause: unit -> unit
        abstract Next: unit -> unit
        abstract Previous: unit -> unit
        abstract Shuffle: unit -> unit
        abstract AddToPlaylistAsync: path:AppFile -> Task<unit>
        abstract RemoveFromPlaylist: index:int -> unit
        abstract CurrentSong: unit -> Task<Option<AppFile>>
        abstract Playlist: list<AppFile>

    module private CrossPlat =
        open LibVLCSharp.Shared

        let initializeCore () = Core.Initialize()

        type CrossPlatPlayer() =
            let libvlc = new LibVLC()
            let player: MediaPlayer = new MediaPlayer(libvlc)

            let shuffle (org: list<AppFile>) =
                let rng = Random()
                let arr = Array.copy (org |> List.toArray)
                let max = (arr.Length - 1)

                let randomSwap (arr: _ []) i =
                    let pos = rng.Next(max)
                    let tmp = arr.[pos]
                    arr.[pos] <- arr.[i]
                    arr.[i] <- tmp
                    arr

                [| 0 .. max |]
                |> Array.fold randomSwap arr
                |> Array.toList

            let mutable playlist: list<AppFile> = list.Empty
            let mutable currentIndex: int = 0
            let mutable currentMedia: Option<AppFile> = None

            interface IPlayer with
                member __.Dispose(): unit =
                    player.Dispose()
                    libvlc.Dispose()

                member __.Play() = player.Play() |> ignore

                member __.Play(index: int) =
                    currentMedia <-
                        match index with
                        | value when value >= 0 -> Some(playlist.[value])
                        | _ -> None
                    match currentMedia with
                    | Some media ->
                        use media =
                            new Media(libvlc, media.Path, FromType.FromPath)

                        player.Play media |> ignore
                    | None -> ()


                member __.Pause() = player.Pause() |> ignore

                member __.Next(): unit =
                    let index = currentIndex + 1
                    currentIndex <- if index < playlist.Length then index else 0
                    currentMedia <-
                        match index with
                        | value when value >= 0 -> Some(playlist.[value])
                        | _ -> None
                    match currentMedia with
                    | Some media ->
                        use media =
                            new Media(libvlc, media.Path, FromType.FromPath)

                        player.Play media |> ignore
                    | None -> ()

                member __.Previous(): unit =
                    let index = currentIndex - 1
                    currentIndex <- if index >= 0 then index else 0
                    currentMedia <-
                        match index with
                        | value when value >= 0 -> Some(playlist.[value])
                        | _ -> None
                    match currentMedia with
                    | Some media ->
                        use media =
                            new Media(libvlc, media.Path, FromType.FromPath)

                        player.Play media |> ignore
                    | None -> ()

                member __.Shuffle(): unit =
                    if playlist.Length > 0 then
                        playlist <- shuffle (playlist)

                        use media =
                            new Media(libvlc, playlist.[0].Path, FromType.FromPath)

                        player.Play(media) |> ignore
                        currentIndex <- 0
                        currentMedia <- Some playlist.[0]

                member __.CurrentSong(): Task<Option<AppFile>> = Task.FromResult(currentMedia)

                member __.AddToPlaylistAsync(file: AppFile) =
                    playlist <- file :: playlist
                    Task.FromResult(())

                member __.RemoveFromPlaylist(index: int) =
                    let el = playlist.Item index
                    playlist <- playlist |> List.filter (fun item -> item <> el)

                member __.Playlist = playlist

    module private Win10 =
        open Windows.Storage
        open Windows.Media
        open Windows.Media.Core
        open Windows.Media.Playback

        type WindowsPlayer() =
            let player = new MediaPlayer()
            let playlist = MediaPlaybackList()
            let mutable _appfilelist: list<AppFile> = list.Empty
            do player.Source <- playlist

            interface IPlayer with
                member __.CurrentSong(): Task<Option<AppFile>> =
                    task {
                        let can, path =
                            playlist.CurrentItem.Source.CustomProperties.TryGetValue "path"

                        if can then
                            let! file = FileSystem.getFileAsync (path :?> string)
                            return Some file
                        else
                            return None
                    }

                member __.Dispose(): unit = player.Dispose()

                member __.Play() = player.Play()

                member __.Play(index: int) =
                    playlist.MoveTo(index |> uint32) |> ignore
                    player.Play()

                member __.Pause() = player.Pause()

                member __.Next(): unit = playlist.MoveNext() |> ignore

                member __.Previous(): unit = playlist.MovePrevious() |> ignore

                member __.Shuffle(): unit =
                    if playlist.ShuffleEnabled then
                        playlist.ShuffleEnabled <- false
                    else
                        playlist.ShuffleEnabled <- true

                member __.AddToPlaylistAsync(appFile: AppFile) =
                    task {
                        let! file = StorageFile.GetFileFromPathAsync(appFile.Path).AsTask()
                        let source = MediaSource.CreateFromStorageFile file
                        let item = MediaPlaybackItem source
                        __.ApplyMusicProps item appFile
                        playlist.Items.Add item
                        _appfilelist <- appFile :: _appfilelist
                    }

                member __.RemoveFromPlaylist(index: int) =
                    playlist.Items.RemoveAt index
                    let el = _appfilelist.Item index
                    _appfilelist <-
                        _appfilelist
                        |> List.filter (fun item -> item <> el)

                member __.Playlist = _appfilelist

            member private __.ApplyMusicProps (item: MediaPlaybackItem) (appFile: AppFile) =
                let props = item.GetDisplayProperties()
                props.Type <- MediaPlaybackType.Music
                props.MusicProperties.Title <-
                    match appFile.Title with
                    | Some value -> value
                    | None -> ""
                props.MusicProperties.AlbumArtist <-
                    match appFile.AlbumArtist with
                    | Some value -> value
                    | None -> ""
                props.MusicProperties.AlbumTitle <-
                    match appFile.AlbumTitle with
                    | Some value -> value
                    | None -> ""
                props.MusicProperties.Artist <-
                    match appFile.Artist with
                    | Some value -> value
                    | None -> ""
                props.MusicProperties.TrackNumber <-
                    match appFile.TrackNumber with
                    | Some value -> value
                    | None -> 0u
                item.ApplyDisplayProperties(props)

    let private getPlatformPlayer (platform: AppPlatform): IPlayer =
        match platform with
        | Linux
        | MacOS ->
            CrossPlat.initializeCore ()
            new CrossPlat.CrossPlatPlayer() :> IPlayer
        | Windows -> new Win10.WindowsPlayer() :> IPlayer

    let getPlayer: Lazy<IPlayer> =
        let platform = getPlatform ()
        lazy (getPlatformPlayer platform)

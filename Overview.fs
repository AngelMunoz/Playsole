namespace Playsole

module Overview =
    open Terminal.Gui
    open Terminal.Gui.Elmish

    type State = { Noop: Option<unit> }

    type Msg = | Noop

    let init () = { Noop = None }

    let update (msg: Msg) (state: State) = state, Cmd.none

    let view (state: State) (dispatch: Msg -> unit): list<View> = []

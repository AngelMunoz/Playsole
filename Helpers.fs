namespace Playsole

module Helpers =
    open Types
    open System.Runtime.InteropServices

    let getPlatform () =
        if RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
        then Windows
        else if RuntimeInformation.IsOSPlatform(OSPlatform.Linux)
        then Linux
        else if RuntimeInformation.IsOSPlatform(OSPlatform.OSX)
        then MacOS
        else failwith "Unsuported Runtime"

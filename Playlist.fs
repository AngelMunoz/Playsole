namespace Playsole

module Playlist =
    open Terminal.Gui
    open Terminal.Gui.Elmish
    open Fs

    type State = { Playlist: list<AppFile> }

    type ExternalMsg = PlaySong of AppFile

    type Msg =
        | UpdatePlaylist of list<AppFile>
        | PlaySong of AppFile

    let init () = { Playlist = list.Empty }, Cmd.none

    let update (msg: Msg) (state: State) =
        match msg with
        | UpdatePlaylist playlist -> { state with Playlist = playlist }, Cmd.none, None
        | PlaySong song -> state, Cmd.none, Some(ExternalMsg.PlaySong song)

    let private childcontent (files: list<AppFile>) (dispatch: Msg -> unit) =
        let content =
            let files =
                files
                |> List.map (fun file -> (file, file.FileName))

            [ Items files
              OnChanged(PlaySong >> dispatch) ]

        listView content

    let view (state: State) (dispatch: Msg -> unit): list<View> = [ childcontent state.Playlist dispatch ]
